package co.zipperstudios.androidsampleapp.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import co.zipperstudios.androidsampleapp.R;

public class SampleActivity extends AppCompatActivity {

    CoordinatorLayout mRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        mRoot = (CoordinatorLayout) findViewById(R.id.as_cl_root);
        Button startSample = (Button) findViewById(R.id.as_btn_start_sample);

        final View.OnClickListener customClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SampleActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        };

        startSample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button clickedButton = (Button) view;
                Snackbar.make(mRoot, clickedButton.getText().toString(), Snackbar.LENGTH_LONG)
                        .setAction(R.string.start, customClickListener)
                        .setActionTextColor(Color.RED)
                        .show();
            }
        });
    }
}
